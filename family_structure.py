import json
import math
from datetime import datetime
from datetime import date
import datetime
import calendar
from pprint import pprint
import numpy as np
import pandas as pd

global mortality_data
mortality_data= pd.read_csv('lookup_mortality_india.csv')
today=datetime.datetime.strptime("2016-07-01", '%Y-%m-%d').date()
global first_date


def add_months(sourcedate,months):
     month = sourcedate.month - 1 + months
     year = int(sourcedate.year + month / 12 )
     month = month % 12 + 1
     day = min(sourcedate.day,calendar.monthrange(year,month)[1])
     return datetime.date(year,month,day)

def first_day_of_month(d):
	return datetime.date(d.year, d.month, 1)

newdate = first_day_of_month(today)
first_date = newdate


def year_diff(d1, d2):
       if d1 == d2:
           return 0
       year = abs(d1.year - d2.year)
       if d1 > d2:
           year -= abs((d1.month, d1.day) < (d2.month, d2.day))
       else:
           year -= abs((d2.month, d2.day) < (d1.month, d1.day))
       return year

def max_iteration_1(data,newdate):
	_max=0
	for_mor=0
	for_dob=""
	for i in data:
		dob=datetime.datetime.strptime(i["dob"], '%Y-%m-%d').date()
		curr_age=year_diff(newdate,dob)
		if (i["relationship"] == "SELF" or i["relationship"] == "SPOUSE") and i["gender"] == "M":
			mortality_age=int(mortality_data.query("age=="+str(curr_age))["male"])
			if mortality_age -curr_age> _max:
				_max =mortality_age-curr_age
				for_mor=mortality_age
				for_dob = dob
		elif (i["relationship"] == "SELF" or i["relationship"] == "SPOUSE") and i["gender"] == "F":
			mortality_age = int(mortality_data.query("age=="+str(curr_age))["female"])
			if mortality_age -curr_age > _max:
				_max =mortality_age-curr_age
				for_mor=mortality_age
				for_dob = dob

	time_of_death = add_months(for_dob,for_mor*12)
	return time_of_death

def max_iteration(data,newdate):
	_max=0
	for_dob=""
	for i in data:
		dob=datetime.datetime.strptime(i["dob"], '%Y-%m-%d').date()
		curr_age=year_diff(newdate,dob)
		#print curr_age
		if (i["relationship"] == "SELF" or i["relationship"] == "SPOUSE") and i["gender"] == "M":
			mortality_age=mortality_data.query("age=="+str(curr_age))
			diff_age=int(mortality_age["male"])-curr_age
			if (diff_age) > _max:
				_max = diff_age
				for_dob=dob
				#_max = max(int(mortality_age["male"])-curr_age, _max)
		elif (i["relationship"] == "SELF" or i["relationship"] == "SPOUSE") and i["gender"] == "F":
			mortality_age=mortality_data.query("age=="+str(curr_age))
			diff_age=int(mortality_age["male"])-curr_age
			if (diff_age) > _max:
				_max = diff_age
				for_dob=dob
			#_max = max(int(mortality_age["female"])-curr_age, _max)

	print "MAX------------------",_max
	return _max

def age_thresholder(data,curr_age):
	dependent_duration = data['dependent_till'].lower()
	dob=datetime.datetime.strptime(data["dob"], '%Y-%m-%d').date()
	age_today= year_diff(today,dob)
	x=""
	#print data["relationship"]
	if dependent_duration == "demise":
		if data["gender"] == "M":
			x = "male"
		else:
			x = "female"
		#print data["relationship"],int(mortality_data.query("age=="+str(age_today))[x]) , x ,age_today
		#print int(mortality_data.query("age=="+str(age_today))[x])
		return mortality_data.query("age=="+str(age_today))[x]
	if dependent_duration == "marriage":
		return data['marriage_age']
	if dependent_duration == "education":
		return data["education"]
	if dependent_duration == "age":
		return data['dependent_till_age']

	return None


def marital_check(data,curr_date):
	count =0 
	for i in data:
		if i["relationship"] == "SELF" or i["relationship"] == "SPOUSE" :
			dob=datetime.datetime.strptime(i["dob"], '%Y-%m-%d').date()
			curr_age=year_diff(curr_date,dob)
			#print int(age_thresholder(i,curr_age))
			if curr_age < int(age_thresholder(i,curr_age)):
				count = count + 1

	if count ==1:
		return "S"
	elif count ==2:
		return "M"			
def kids_check(data,curr_date):
	count=0
	for i in data:
		if not(i["relationship"] == "SELF" or i["relationship"] == "SPOUSE"):
			dob=datetime.datetime.strptime(i["dob"], '%Y-%m-%d').date()
			curr_age=year_diff(curr_date,dob)
			if curr_age <18:
				count = count + 1

	if count >=3:
		return "3+"
	elif count ==0:
		return "N"
	elif count <3:
		return "<3" 

def income_check(data,curr_date):
	count =0
	for i in data:
		#print i["relationship"]
		if (i["relationship"] == "SELF" or i["relationship"] == "SPOUSE") and i["working"] == True : ##to be checked
			#print i["relationship"]
			dob=datetime.datetime.strptime(i["dob"], '%Y-%m-%d').date()
			curr_age=year_diff(curr_date,dob)
			if curr_age <= i["retirement_age"]:
				count = count +1

	if count == 0:
		return "R"
	elif count == 1:
		return "S"
	elif count == 2:
		return "D"

def dependent_check(data,curr_date):
	count =0
	for i in data:
		dob=datetime.datetime.strptime(i["dob"], '%Y-%m-%d').date()
		curr_age=year_diff(curr_date,dob)
		if curr_age>=18:
			if not(i["relationship"] == "SELF" or i["relationship"] == "SPOUSE"):
				if curr_age<int(age_thresholder(i,curr_age)):
					count = count + 1

	if count>0 :
		return "Y"
	else:
		return "N"

def family_size_check(data,curr_date):
	count =0
	for i in data:
		dob=datetime.datetime.strptime(i["dob"], '%Y-%m-%d').date()
		curr_age=year_diff(curr_date,dob)
		#print str(i["relationship"]),curr_age
		if curr_age<int(age_thresholder(i,curr_age)):
			count=count +1
	return count




with open('family_structures.json') as data_file:    
    data = json.load(data_file)

for z in range(0,len(data)):
	print "This is iterartion no" + str(z)
	#print data[z]["members"]
	times_to_iterate = max_iteration_1(data[z]["members"],today)
	#print "The mortality age is :" + str(mortality_data.query("age=="+str(curr_age)))
	family_structures={}
	family_sizes = {}
	#newdate = add_months(newdate,1)
	#print times_to_iterate
	while newdate < times_to_iterate:
		mar=marital_check(data[z]["members"],newdate)
		kid=kids_check(data[z]["members"],newdate)
		dep=dependent_check(data[z]["members"],newdate)
		inc=income_check(data[z]["members"],newdate)
		final = "%s-%s-%s-%s" %(mar,kid,dep,inc)
		family_mem = family_size_check(data[z]["members"],newdate)
		family_sizes[str(newdate)]=family_mem
		family_structures[str(newdate)]=final
		#print newdate
		newdate = add_months(newdate,1)
		#print family_mem
		#print final
		#print "  "
	data[z]["family_sizes"]=family_sizes
	data[z]["family_structures"]=family_structures
	newdate = first_date

with open('family_code_result.json', 'w') as fp:
     json.dump(data, fp)